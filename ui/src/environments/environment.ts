export const environment = {
  production: false,
  gateway: 'https://localhost:3001',
  url: 'https://localhost:4200',
  callback: 'https://localhost:4200/callback',
  domain: 'dev-dogh3506.eu.auth0.com',
  clientId: 'EDncLCaZcSxQGyYthz8pltC7Qtqk0F8d',
  audience: 'http://myorganizer.com'
};
