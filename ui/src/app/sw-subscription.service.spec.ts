import { TestBed } from '@angular/core/testing';

import { SwSubscriptionService } from './sw-subscription.service';

describe('SwSubscriptionService', () => {
  let service: SwSubscriptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SwSubscriptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
