import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  isWide = true;

  constructor(public auth: AuthService) { }

  ngOnInit() {
    if (window.innerWidth < 550) {
      this.isWide = false;
    }
  }

}
