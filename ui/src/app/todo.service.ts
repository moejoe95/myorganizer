import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class TodoService {
  constructor(private httpClient: HttpClient) { }

  getTodoList(userId: string) {
    return this.httpClient.get(environment.gateway + '/todo/' + userId);
  }

  addTodo(todo: Todo) {
    return this.httpClient.post(environment.gateway + '/todo', todo);
  }

  updateTodo(todo: Todo) {
    return this.httpClient.put(environment.gateway + '/todo', todo);
  }

  deleteTodo(todo: Todo) {
    return this.httpClient.delete(environment.gateway + '/todo/' + todo.id);
  }
}

export class Todo {
  id: string;
  message: string;
  complete: boolean;
  deadline: Date;
  subscriptionId: string;
  userId: string;
  tags: Tag[];
  deleted: boolean;
}

export interface Tag {
  id: string;
  name: string;
}
