import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker'
import { Todo, TodoService } from './todo.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class SwSubscriptionService {

  readonly VAPID_PUBLIC_KEY = "BEI29qAAlTGinxOeTKsK7WwnUSqX2U5ISTTtJjF0_nnapfI8H8yAPTWSDW09xXMVweUZxzI8bNKW1YBVQg2_zUo";

  constructor(private swPush: SwPush, private httpClient: HttpClient, private todoService: TodoService) { }

  subscribe(todo: Todo) {
    this.swPush.requestSubscription({
      serverPublicKey: this.VAPID_PUBLIC_KEY
    })
      .then(sub => {
        var subscription: Subscription = {
          id: uuid.v4(),
          url: sub.endpoint,
          subscription: JSON.stringify(sub.toJSON())
        };

        this.addSubscription(subscription).subscribe(() => {
          todo.subscriptionId = subscription.id;
          this.todoService.updateTodo(todo).subscribe(() => {
          });
        });


      })
      .catch(err => console.error("Could not subscribe to notifications", err));
  }

  addSubscription(subscription: Subscription) {
    return this.httpClient.post(environment.gateway + '/subscription', subscription);
  }
}

export class Subscription {
  id: string;
  url: string;
  subscription: string;
}
