import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

import * as auth0 from 'auth0-js';

//https://auth0.com/docs/libraries/auth0js

(window as any).global = window;

@Injectable()
export class AuthService {
  constructor(public router: Router) { }

  accessToken: string;
  idToken: string;
  expires: number;
  userId: string;
  userName: string;

  auth0 = new auth0.WebAuth({
    clientID: environment.clientId,
    domain: environment.domain,
    responseType: 'token id_token',
    audience: environment.audience,
    redirectUri: environment.callback,
    scope: 'openid'
  });

  public login(): void {
    this.auth0.authorize();
  }

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (err) console.log(err);
      if (!err && authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
      }
      this.router.navigate(['/home']);
    });
  }

  private setSession(authResult): void {
    // Set the time that the Access Token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    this.accessToken = authResult.accessToken;
    this.idToken = authResult.idToken;
    this.expires = parseInt(expiresAt);
    this.auth0.client.userInfo(authResult.accessToken, (err, user) => {
      if (err) console.log(err);
      this.userId = user.sub;
    });
  }

  public logout(): void {
    this.auth0.logout({
      returnTo: environment.url,
      clientID: environment.clientId
    });
  }

  public isAuthenticated(): boolean {
    return new Date().getTime() < this.expires;
  }

  public createAuthHeaderValue(): string {
    return 'Bearer ' + this.accessToken;
  }
}
