import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private httpClient: HttpClient) {
  }

  createGroup(group: Group) {
    return this.httpClient.post(environment.gateway + '/group', group);
  }

  getGroups(userId: string) {
    return this.httpClient.get(environment.gateway + '/group/' + userId);
  }

  deleteGroup(group: Group) {
    return this.httpClient.delete(environment.gateway + '/group/' + group.id + '/' + group.userId)
  }
}

export class Group {
  id: string;
  name: string;
  userId: string;
}
