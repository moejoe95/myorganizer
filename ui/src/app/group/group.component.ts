import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as uuid from 'uuid';
import { AuthService } from '../auth.service';
import { Group, GroupService } from '../group.service';
import { ClipboardService } from 'ngx-clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TodoTableComponent } from '../todo-table/todo-table.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddGroupDialogComponent } from '../add-group-dialog/add-group-dialog.component';
import { JoinGroupDialogComponent } from '../join-group-dialog/join-group-dialog.component';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  groups: Group[] = [];
  joinId: string;
  groupId: string;
  groupName: string;
  groupSet: boolean = false;

  @ViewChild('todoTable') todoTable: TodoTableComponent;
  activeTodosFilter = (a) => !a.complete;

  constructor(private groupService: GroupService, private authService: AuthService, private clipboardApi: ClipboardService, private snackBar: MatSnackBar, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getGroups();
  }

  openAddDialog(): void {
    var newGroup: Group = {
      id: uuid.v4(),
      userId: this.authService.userId,
      name: null
    };
    const dialogRef = this.dialog.open(AddGroupDialogComponent, {
      width: '250px',
      data: newGroup,
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getGroups();
    });
  }

  openJoinDialog(): void {
    var group: Group = {
      id: this.joinId,
      name: null,
      userId: this.authService.userId
    }
    const dialogRef = this.dialog.open(JoinGroupDialogComponent, {
      width: '250px',
      data: group,
    });

    this.groupService.createGroup(group).subscribe(() => {
      this.getGroups();
    });
  }

  getGroups(): void {
    this.groupService.getGroups(this.authService.userId).subscribe((data: Group[]) => {
      this.groups = data;
    });
  }

  copyLink(group: Group): void {
    this.clipboardApi.copyFromContent(group.id);

    this.snackBar.open("Copied ID to clipboard", "Copied", {
      duration: 3000,
    });
  }

  onSetGroup(group: Group) {
    if (group.id === this.groupId) {
      this.groupSet = false;
      this.groupId = null;
    } else {
      this.groupName = group.name;
      this.groupSet = true;
      this.groupId = group.id;
      this.todoTable.userId = this.groupId;
      this.todoTable.getAll();
    }
  }

  deleteGroup(group: Group) {
    this.groupService.deleteGroup(group).subscribe(() => {
      this.getGroups();
    })
  }

}
