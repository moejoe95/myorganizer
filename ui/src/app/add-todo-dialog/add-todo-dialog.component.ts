import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tag, Todo } from '../todo.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import * as uuid from 'uuid';

@Component({
  selector: 'app-add-todo-dialog',
  templateUrl: './add-todo-dialog.component.html',
  styleUrls: ['./add-todo-dialog.component.css']
})
export class AddTodoDialogComponent implements OnInit {

  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  removable = true;
  selectable = true;
  addOnBlur = true;

  constructor(
    public dialogRef: MatDialogRef<AddTodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Todo) {
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addTodo() {
    this.dialogRef.close();
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const tagName = value.trim().toLowerCase();
      this.data.tags.push({ name: tagName, id: uuid.v4() });
    }
    if (input) {
      input.value = '';
    }
  }

  removeTag(fruit: Tag): void {
    const index = this.data.tags.indexOf(fruit);

    if (index >= 0) {
      this.data.tags.splice(index, 1);
    }
  }
}
