import { Component, Input, OnInit } from '@angular/core';
import { TodoService, Todo } from '../todo.service';
import { SwSubscriptionService } from '../sw-subscription.service'
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import * as uuid from 'uuid';
import { AddTodoDialogComponent } from '../add-todo-dialog/add-todo-dialog.component';

@Component({
  selector: 'app-todo-table',
  templateUrl: './todo-table.component.html',
  styleUrls: ['./todo-table.component.css']
})
export class TodoTableComponent implements OnInit {

  @Input() userId: string;

  title: string;
  alternativeTitle: string;

  todosDs = new MatTableDataSource();
  displayedColumns: string[] = ['message', 'tags', 'date', 'actions'];

  showOpenTitle = "Open";
  showCompletedTitle = "Completed";
  activeTodosFilter = (a) => !a.complete;
  completedTodosFilter = (a) => a.complete;
  filter: any;
  showCompleted = true;


  constructor(private todoService: TodoService, private swService: SwSubscriptionService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.filter = this.activeTodosFilter;
    this.title = this.showOpenTitle;
    this.alternativeTitle = this.showCompletedTitle;
    this.getAll();
    this.todosDs.filterPredicate = this.tagFilter;
    if (window.innerWidth < 500) {
      this.displayedColumns.splice(2, 1);
    }
  }

  tagFilter(data: Todo, filter: string): boolean {
    var tagString = '';
    data.tags.forEach(tag => {
      tagString += tag.name;
    });

    var tagContains = tagString.toLowerCase().indexOf(filter) !== -1;
    var msgContains = data.message.toLowerCase().indexOf(filter) !== -1;
    return tagContains || msgContains;
  }

  getAll() {
    this.todoService.getTodoList(this.userId).subscribe((data: Todo[]) => {
      var todos = data.filter(this.filter);
      this.todosDs.data = todos;
    });
  }

  completeTodo(todo: Todo) {
    todo.complete = true;
    this.todoService.updateTodo(todo).subscribe(() => {
      this.getAll();
    });
  }

  deleteTodo(todo: Todo) {
    todo.deleted = true;
    this.todoService.updateTodo(todo).subscribe(() => {
      this.getAll();
    })
  }

  subscribeToSw(todo: Todo) {
    this.swService.subscribe(todo);
  }


  applyFilter(filterValue: string) {
    this.todosDs.filter = filterValue.trim().toLowerCase();
  }

  searchChip(name: string) {
    var oldValue = (<HTMLInputElement>document.getElementById("filterInput")).value;
    if (oldValue === name) {
      name = '';
    }
    this.applyFilter(name);
    (<HTMLInputElement>document.getElementById("filterInput")).value = name;
  }

  openAddDialog(): void {
    var newTodo: Todo = {
      message: '',
      id: uuid.v4(),
      complete: false,
      deadline: null,
      subscriptionId: '',
      userId: this.userId,
      tags: [],
      deleted: false
    };
    const dialogRef = this.dialog.open(AddTodoDialogComponent, {
      width: '250px',
      data: newTodo
    });

    dialogRef.afterClosed().subscribe(result => {
      if (newTodo.message !== '') {
        this.todoService.addTodo(newTodo).subscribe(() => {
          this.getAll();
        })
      }
    });
  }

  switchComplete() {
    if (this.showCompleted) {
      this.filter = this.completedTodosFilter;
      this.title = this.showCompletedTitle;
      this.alternativeTitle = this.showOpenTitle;
    } else {
      this.filter = this.activeTodosFilter;
      this.title = this.showOpenTitle;
      this.alternativeTitle = this.showCompletedTitle;
    }
    this.getAll();
    this.showCompleted = !this.showCompleted;
  }

}
