package todo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"myorganizer/subscription"
	"os"

	"github.com/SherClockHolmes/webpush-go"
	"github.com/jinzhu/gorm"

	_ "github.com/mattn/go-sqlite3"
)

var (
	vapidPublic  string
	vapidPrivate string
	db           *gorm.DB
)

func init() {
	vapidPublic = os.Getenv("VAPID_PUBLIC_KEY")
	vapidPrivate = os.Getenv("VAPID_PRIVATE_KEY")
	if vapidPrivate == "" {
		data, err := ioutil.ReadFile("/run/secrets/vapid_key")
		if err != nil {
			panic("failed to get private vapid key")
		}
		vapidPrivate = string(data)
	}

	database, err := gorm.Open("sqlite3", "/db/myorganizer.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}

	// create tables if not there
	database.AutoMigrate(&Todo{}, &Tag{})

	db = database
}

// GetAll retrieves all elements from todos
func GetAll(userId string) []Todo {
	var todos []Todo
	db.Preload("Tags").Where("deleted = ? and user_id = ?", false, userId).Find(&todos)
	return todos
}

// GetOne retrieves one element from todos
func GetOne(id string) Todo {
	var todo Todo
	db.Preload("Tags").Where("id = ?", id).Find(&todo)
	return todo
}

// Add will add a new todo based on a message
func Add(todoItem Todo) string {
	db.Create(&todoItem)
	return todoItem.ID
}

// Delete will remove a Todo from the Todo list
func Delete(id string) {
	var todo Todo
	db.Where("id = ?", id).Find(&todo)
	db.Delete(&todo)
}

// Update todo
func Update(todoItem Todo) {
	var todo Todo
	db.Where("id = ?", todoItem.ID).Find(&todo)
	todo.Complete = todoItem.Complete
	todo.SubscriptionID = todoItem.SubscriptionID
	todo.Deleted = todoItem.Deleted
	db.Save(&todo)
}

func getWithSubscriptions() []Todo {
	var todos []Todo
	db.Where("subscription_id is not ? and complete = ?", "", 0).Find(&todos)
	return todos
}

// Notify notify subscribers
func Notify() {
	todos := getWithSubscriptions()

	for _, todo := range todos {
		sub := subscription.GetOne(todo.SubscriptionID)
		s := &webpush.Subscription{}
		json.Unmarshal([]byte(sub.JSON), s)

		notification := subscription.Notification{
			Title: "Todo notification",
			Body:  todo.Message,
		}

		payload := subscription.NotificationPayload{
			Payload: notification,
		}

		reqBodyBytes := new(bytes.Buffer)
		json.NewEncoder(reqBodyBytes).Encode(payload)

		// Send Notification
		_, err := webpush.SendNotification(reqBodyBytes.Bytes(), s, &webpush.Options{
			Subscriber:      sub.URL,
			VAPIDPublicKey:  vapidPublic,
			VAPIDPrivateKey: vapidPrivate,
			TTL:             30,
		})

		if err != nil {
			fmt.Println(err)
		} else {
			todo.SubscriptionID = ""
			db.Save(&todo)
			db.Delete(sub)
		}
	}

}
