package todo

// Tag data structure for tagging todos
type Tag struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	TodoID string `json:"todo_id"`
}
