package todo

import (
	"time"
)

// Todo data structure for task with a description of what to do
type Todo struct {
	ID             string    `json:"id"`
	Message        string    `json:"message"`
	Complete       bool      `json:"complete"`
	Deadline       time.Time `json:"deadline"`
	SubscriptionID string    `json:"subscriptionId"`
	UserID         string    `json:"userId"`
	Tags           []Tag     `json:"tags"`
	Deleted        bool      `json:"deleted"`
}
