package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"log"
	"math/big"
	"myorganizer/handlers"
	"myorganizer/todo"
	"time"

	"net/http"
	"os"
	"path"
	"path/filepath"

	"github.com/auth0-community/go-auth0"
	"github.com/gin-gonic/gin"
	"github.com/prprprus/scheduler"
	jose "gopkg.in/square/go-jose.v2"
)

var (
	audience string
	domain   string
)

func main() {

	go schedule()

	setAuth0Variables()
	r := gin.Default()
	r.Use(CORSMiddleware())

	// This will ensure that the angular files are served correctly
	r.NoRoute(func(c *gin.Context) {
		dir, file := path.Split(c.Request.RequestURI)
		ext := filepath.Ext(file)
		if file == "" || ext == "" {
			c.File("./ui/dist/ui/index.html")
		} else {
			c.File("./ui/dist/ui/" + path.Join(dir, file))
		}
	})

	authorized := r.Group("/")
	authorized.Use(authRequired())
	authorized.GET("/todo/:userId", handlers.GetTodoListHandler)
	authorized.POST("/todo", handlers.AddTodoHandler)
	authorized.DELETE("/todo/:id", handlers.DeleteTodoHandler)
	authorized.PUT("/todo", handlers.CompleteTodoHandler)
	authorized.POST("/subscription", handlers.AddSubscriptionHandler)
	authorized.GET("/subscription", handlers.GetSubscriptionListHandler)
	authorized.GET("/group/:id", handlers.GetGroupListHandler)
	authorized.POST("/group", handlers.AddGroupHandler)
	authorized.DELETE("group/:id/:userId", handlers.DeleteGroupHandler)

	// generate self signed certificate
	generateCert()

	r.RunTLS(":3001", "/cert/cert.crt", "/cert/key.pem")
}

func schedule() {
	s, err := scheduler.NewScheduler(1000)
	if err != nil {
		panic(err)
	}

	s.Every().Second(1).Do(todo.Notify)
}

func setAuth0Variables() {
	audience = os.Getenv("AUTH0_API_IDENTIFIER")
	domain = os.Getenv("AUTH0_DOMAIN")
}

// ValidateRequest will verify that a token received from an http request
// is valid and signy by authority
func authRequired() gin.HandlerFunc {
	return func(c *gin.Context) {

		var auth0Domain = "https://" + domain + "/"
		client := auth0.NewJWKClient(auth0.JWKClientOptions{URI: auth0Domain + ".well-known/jwks.json"}, nil)
		configuration := auth0.NewConfiguration(client, []string{audience}, auth0Domain, jose.RS256)
		validator := auth0.NewValidator(configuration, nil)

		_, err := validator.ValidateRequest(c.Request)
		if err != nil {
			log.Println(err)
			terminateWithError(http.StatusUnauthorized, "token is not valid", c)
			return
		}
		c.Next()
	}
}

func terminateWithError(statusCode int, message string, c *gin.Context) {
	c.JSON(statusCode, gin.H{"error": message})
	c.Abort()
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "DELETE, GET, OPTIONS, POST, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func generateCert() {

	if _, err := os.Stat("/cert/cert.crt"); err == nil {
		log.Output(0, "certifacte already exists")

	} else if os.IsNotExist(err) {
		log.Output(0, "generate certifacte")
		privateKey, genKeyErr := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
		if genKeyErr != nil {
			log.Fatalf("Failed to generate private key: %v", genKeyErr)
		}

		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serialNumber, _ := rand.Int(rand.Reader, serialNumberLimit)

		template := x509.Certificate{
			SerialNumber: serialNumber,
			Subject: pkix.Name{
				Organization: []string{"MyOrganizer"},
			},
			DNSNames:  []string{"localhost"},
			NotBefore: time.Now(),
			NotAfter:  time.Now().Add(24 * 365 * time.Hour),

			KeyUsage:              x509.KeyUsageDigitalSignature,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
			BasicConstraintsValid: true,
		}

		derBytes, createCertErr := x509.CreateCertificate(rand.Reader, &template, &template, &privateKey.PublicKey, privateKey)
		if createCertErr != nil {
			log.Fatalf("Failed to create certificate: %v", createCertErr)
		}

		pemCert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
		if err := os.WriteFile("/cert/cert.crt", pemCert, 0644); err != nil {
			log.Fatal(err)
		}

		privBytes, _ := x509.MarshalPKCS8PrivateKey(privateKey)
		pemKey := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: privBytes})
		if pemKey == nil {
			log.Fatal("Failed to encode key to PEM")
		}
		if err := os.WriteFile("/cert/key.pem", pemKey, 0600); err != nil {
			log.Fatal(err)
		}
		log.Output(0, "successfully generated key pair")
	} else {
		log.Panic(err)
	}
}
