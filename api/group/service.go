package group

import (
	"fmt"

	"github.com/jinzhu/gorm"

	_ "github.com/mattn/go-sqlite3"
)

var db *gorm.DB

func init() {
	database, err := gorm.Open("sqlite3", "/db/myorganizer.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	database.AutoMigrate(&Group{})
	db = database
}

// GetAll retrieves all elements from the subscriptions
func GetAll(userId string) []Group {
	var groups []Group
	db.Where("user_id = ?", userId).Find(&groups)
	return groups
}

// GetOne retrieves one group
func GetOne(id string) Group {
	var group Group
	db.Where("id = ?", id).Find(&group)
	return group
}

// Add new group
func Add(group Group) string {
	if len(group.Name) == 0 {
		// if no name given, we join the group
		var groups []Group
		db.Where("id = ?", group.ID).Find(&groups)
		if len(groups) > 0 {
			group.Name = groups[0].Name
		}
	}
	db.Create(&group)
	return group.ID
}

// Delete will remove a Todo from the Todo list
func Delete(id string, userId string) {
	var group Group
	db.Where("id = ? and user_id = ?", id, userId).Delete(&group)
}
