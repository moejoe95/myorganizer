package handlers

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"myorganizer/group"
	"myorganizer/subscription"
	"myorganizer/todo"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetTodoListHandler returns all current todo items
func GetTodoListHandler(c *gin.Context) {
	userId := c.Param("userId")
	c.JSON(http.StatusOK, todo.GetAll(userId))
}

// AddTodoHandler adds a new todo to the todo list
func AddTodoHandler(c *gin.Context) {
	todoItem, statusCode, err := convertHTTPBodyToTodo(c.Request.Body)
	if err != nil {
		c.JSON(statusCode, err)
		return
	}
	c.JSON(statusCode, gin.H{"id": todo.Add(todoItem)})
}

// DeleteTodoHandler will delete a specified todo based on user http input
func DeleteTodoHandler(c *gin.Context) {
	todoID := c.Param("id")
	todo.Delete(todoID)
	c.JSON(http.StatusOK, "")
}

// CompleteTodoHandler will complete a specified todo based on user http input
func CompleteTodoHandler(c *gin.Context) {
	todoItem, statusCode, err := convertHTTPBodyToTodo(c.Request.Body)
	if err != nil {
		c.JSON(statusCode, err)
		return
	}
	todo.Update(todoItem)
	c.JSON(http.StatusOK, "")
}

func convertHTTPBodyToTodo(httpBody io.ReadCloser) (todo.Todo, int, error) {
	body, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return todo.Todo{}, http.StatusInternalServerError, err
	}
	defer httpBody.Close()
	return convertJSONBodyToTodo(body)
}

func convertJSONBodyToTodo(jsonBody []byte) (todo.Todo, int, error) {
	var todoItem todo.Todo
	err := json.Unmarshal(jsonBody, &todoItem)
	if err != nil {
		return todo.Todo{}, http.StatusBadRequest, err
	}
	return todoItem, http.StatusOK, nil
}

// GetSubscriptionListHandler returns all current subscription items
func GetSubscriptionListHandler(c *gin.Context) {
	c.JSON(http.StatusOK, subscription.GetAll())
}

// AddSubscriptionHandler adds a subscription
func AddSubscriptionHandler(c *gin.Context) {
	sub, statusCode, err := convertHTTPBodyToSubscription(c.Request.Body)
	if err != nil {
		c.JSON(statusCode, err)
		return
	}
	subscription.Add(sub)
	c.JSON(http.StatusOK, "")
}

func convertHTTPBodyToSubscription(httpBody io.ReadCloser) (subscription.Subscription, int, error) {
	body, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return subscription.Subscription{}, http.StatusInternalServerError, err
	}
	defer httpBody.Close()
	return convertJSONBodyToSubscription(body)
}

func convertJSONBodyToSubscription(jsonBody []byte) (subscription.Subscription, int, error) {
	var subscriptionItem subscription.Subscription
	err := json.Unmarshal(jsonBody, &subscriptionItem)
	if err != nil {
		return subscription.Subscription{}, http.StatusBadRequest, err
	}
	return subscriptionItem, http.StatusOK, nil
}

// GetGroupListHandler returns all current sgroups
func GetGroupListHandler(c *gin.Context) {
	userId := c.Param("id")
	c.JSON(http.StatusOK, group.GetAll(userId))
}

// DeleteTodoHandler will delete a specified todo based on user http input
func DeleteGroupHandler(c *gin.Context) {
	groupId := c.Param("id")
	userId := c.Param("userId")
	group.Delete(groupId, userId)
	c.JSON(http.StatusOK, "")
}

// AddGroupHandler adds a new group
func AddGroupHandler(c *gin.Context) {
	gr, statusCode, err := convertHTTPBodyToGroup(c.Request.Body)
	if err != nil {
		c.JSON(statusCode, err)
		return
	}
	group.Add(gr)
	c.JSON(http.StatusOK, "")
}

func convertHTTPBodyToGroup(httpBody io.ReadCloser) (group.Group, int, error) {
	body, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return group.Group{}, http.StatusInternalServerError, err
	}
	defer httpBody.Close()
	return convertJSONBodyToGroup(body)
}

func convertJSONBodyToGroup(jsonBody []byte) (group.Group, int, error) {
	var groupItem group.Group
	err := json.Unmarshal(jsonBody, &groupItem)
	if err != nil {
		return group.Group{}, http.StatusBadRequest, err
	}
	return groupItem, http.StatusOK, nil
}
