module myorganizer

go 1.16

require (
	github.com/SherClockHolmes/webpush-go v1.1.2
	github.com/auth0-community/go-auth0 v1.0.0
	github.com/gin-gonic/gin v1.6.3
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/prprprus/scheduler v0.5.0 // indirect
	github.com/rs/xid v1.2.1
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1
)
