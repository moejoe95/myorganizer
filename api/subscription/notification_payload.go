package subscription

// NotificationPayload for service worker
type NotificationPayload struct {
	Payload Notification `json:"notification"`
}
