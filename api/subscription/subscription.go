package subscription

// Subscription for service worker
type Subscription struct {
	ID   string `json:"id"`
	URL  string `json:"url"`
	JSON string `json:"subscription"`
}
