package subscription

import (
	"fmt"

	"github.com/jinzhu/gorm"

	_ "github.com/mattn/go-sqlite3"
)

var db *gorm.DB

func init() {
	database, err := gorm.Open("sqlite3", "/db/myorganizer.db")
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect database")
	}
	database.AutoMigrate(&Subscription{})
	db = database
}

// GetAll retrieves all elements from the subscriptions
func GetAll() []Subscription {
	var subscriptions []Subscription
	db.Find(&subscriptions)
	return subscriptions
}

// GetOne retrieves one elements from the subscriptions
func GetOne(id string) Subscription {
	var subscription Subscription
	db.Where("id = ?", id).Find(&subscription)
	return subscription
}

// Add service worker subscription
func Add(subscription Subscription) string {
	db.Create(&subscription)
	return subscription.ID
}
