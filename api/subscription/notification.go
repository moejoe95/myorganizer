package subscription

// Notification for service worker
type Notification struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}
